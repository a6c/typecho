-- phpMyAdmin SQL Dump
-- version 4.0.10.19
-- https://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2018-01-01 22:23:15
-- 服务器版本: 5.5.54-log
-- PHP 版本: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `typecho`
--

-- --------------------------------------------------------

--
-- 表的结构 `typecho_comments`
--

CREATE TABLE IF NOT EXISTS `typecho_comments` (
  `coid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned DEFAULT '0',
  `created` int(10) unsigned DEFAULT '0',
  `author` varchar(200) DEFAULT NULL,
  `authorId` int(10) unsigned DEFAULT '0',
  `ownerId` int(10) unsigned DEFAULT '0',
  `mail` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `agent` varchar(200) DEFAULT NULL,
  `text` text,
  `type` varchar(16) DEFAULT 'comment',
  `status` varchar(16) DEFAULT 'approved',
  `parent` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`coid`),
  KEY `cid` (`cid`),
  KEY `created` (`created`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `typecho_comments`
--

INSERT INTO `typecho_comments` (`coid`, `cid`, `created`, `author`, `authorId`, `ownerId`, `mail`, `url`, `ip`, `agent`, `text`, `type`, `status`, `parent`) VALUES
(2, 21, 1514533834, 'test', 0, 1, 'test@qq.com', NULL, '172.16.1.167', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36', 'test', 'comment', 'approved', 0),
(3, 21, 1514534107, 'test', 0, 1, 'test@qq.com', NULL, '172.16.1.167', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36', 'test123', 'comment', 'approved', 0),
(4, 20, 1514536972, 'roadkiller', 1, 1, 'webmaster@yourdomain.com', 'http://www.typecho.org', '172.16.1.167', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36', 'test', 'comment', 'approved', 0);

-- --------------------------------------------------------

--
-- 表的结构 `typecho_contents`
--

CREATE TABLE IF NOT EXISTS `typecho_contents` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `created` int(10) unsigned DEFAULT '0',
  `modified` int(10) unsigned DEFAULT '0',
  `text` longtext,
  `order` int(10) unsigned DEFAULT '0',
  `authorId` int(10) unsigned DEFAULT '0',
  `template` varchar(32) DEFAULT NULL,
  `type` varchar(16) DEFAULT 'post',
  `status` varchar(16) DEFAULT 'publish',
  `password` varchar(32) DEFAULT NULL,
  `commentsNum` int(10) unsigned DEFAULT '0',
  `allowComment` char(1) DEFAULT '0',
  `allowPing` char(1) DEFAULT '0',
  `allowFeed` char(1) DEFAULT '0',
  `parent` int(10) unsigned DEFAULT '0',
  `views` int(10) DEFAULT '0',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `slug` (`slug`),
  KEY `created` (`created`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- 转存表中的数据 `typecho_contents`
--

INSERT INTO `typecho_contents` (`cid`, `title`, `slug`, `created`, `modified`, `text`, `order`, `authorId`, `template`, `type`, `status`, `password`, `commentsNum`, `allowComment`, `allowPing`, `allowFeed`, `parent`, `views`) VALUES
(3, 'fuli1', '3', 1513866960, 1514796958, '<!--markdown-->test 12345\r\n\r\n', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(4, 'fuli2', '4', 1513867020, 1514796997, '<!--markdown-->test 12345', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(22, '投稿测试1', '22', 1514538840, 1514796847, '<!--markdown-->投稿测试呵呵\r\n\r\n', 0, 14, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(6, 'Desert.jpg', 'Desert-jpg', 1513950547, 1513950547, 'a:5:{s:4:"name";s:10:"Desert.jpg";s:4:"path";s:35:"/usr/uploads/2017/12/2128899535.jpg";s:4:"size";i:845941;s:4:"type";s:3:"jpg";s:4:"mime";s:10:"image/jpeg";}', 1, 1, NULL, 'attachment', 'publish', NULL, 0, '1', '0', '1', 0, 0),
(14, 'fuli5', '14', 1513952280, 1513952280, '<!--markdown-->fuli5![aed3c46102037c7ad1232c0dd0907411c1f6fb8e1683b-wy4YPY.jpg][1]\r\n\r\n\r\n  [1]: http://172.16.1.167/usr/uploads/2017/12/222179285.jpg', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(9, 'fuli3', '9', 1513951740, 1513951965, '<!--markdown-->fuli3\r\n\r\n\r\n![2cd7f50ee6d2bfeafa808de3ac1aa705db2ad3611035b-I7MJE6.jpg][1]\r\n\r\n\r\n  [1]: http://172.16.1.167/usr/uploads/2017/12/1665005950.jpg', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(10, '2cd7f50ee6d2bfeafa808de3ac1aa705db2ad3611035b-I7MJE6.jpg', '2cd7f50ee6d2bfeafa808de3ac1aa705db2ad3611035b-I7MJE6-jpg-1', 1513951956, 1513951956, 'a:5:{s:4:"name";s:56:"2cd7f50ee6d2bfeafa808de3ac1aa705db2ad3611035b-I7MJE6.jpg";s:4:"path";s:35:"/usr/uploads/2017/12/1665005950.jpg";s:4:"size";i:66395;s:4:"type";s:3:"jpg";s:4:"mime";s:10:"image/jpeg";}', 1, 1, NULL, 'attachment', 'publish', NULL, 0, '1', '0', '1', 9, 0),
(11, '4d8e4bbe6b86802b58e7a8ea811165c737827afb98b1-A0k2mp.jpg', '4d8e4bbe6b86802b58e7a8ea811165c737827afb98b1-A0k2mp-jpg', 1513952008, 1513952008, 'a:5:{s:4:"name";s:55:"4d8e4bbe6b86802b58e7a8ea811165c737827afb98b1-A0k2mp.jpg";s:4:"path";s:35:"/usr/uploads/2017/12/3988171163.jpg";s:4:"size";i:39089;s:4:"type";s:3:"jpg";s:4:"mime";s:10:"image/jpeg";}', 1, 1, NULL, 'attachment', 'publish', NULL, 0, '1', '0', '1', 12, 0),
(12, 'fuli4', '12', 1513951980, 1513952076, '<!--markdown-->![4d8e4bbe6b86802b58e7a8ea811165c737827afb98b1-A0k2mp.jpg][1]\r\n\r\n\r\n  [1]: http://172.16.1.167/usr/uploads/2017/12/3988171163.jpg', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(13, 'aed3c46102037c7ad1232c0dd0907411c1f6fb8e1683b-wy4YPY.jpg', 'aed3c46102037c7ad1232c0dd0907411c1f6fb8e1683b-wy4YPY-jpg', 1513952273, 1513952273, 'a:5:{s:4:"name";s:56:"aed3c46102037c7ad1232c0dd0907411c1f6fb8e1683b-wy4YPY.jpg";s:4:"path";s:34:"/usr/uploads/2017/12/222179285.jpg";s:4:"size";i:92219;s:4:"type";s:3:"jpg";s:4:"mime";s:10:"image/jpeg";}', 1, 1, NULL, 'attachment', 'publish', NULL, 0, '1', '0', '1', 14, 0),
(15, 'c2bffa95a794f42ec3e8c1c50f3f619d4ad55179dffa-uKAiaW.jpg', 'c2bffa95a794f42ec3e8c1c50f3f619d4ad55179dffa-uKAiaW-jpg', 1513952307, 1513952307, 'a:5:{s:4:"name";s:55:"c2bffa95a794f42ec3e8c1c50f3f619d4ad55179dffa-uKAiaW.jpg";s:4:"path";s:35:"/usr/uploads/2017/12/1290185192.jpg";s:4:"size";i:57338;s:4:"type";s:3:"jpg";s:4:"mime";s:10:"image/jpeg";}', 1, 1, NULL, 'attachment', 'publish', NULL, 0, '1', '0', '1', 16, 0),
(16, 'fuli6', '16', 1513952313, 1513952313, '<!--markdown-->fuli6\r\n\r\n\r\n![c2bffa95a794f42ec3e8c1c50f3f619d4ad55179dffa-uKAiaW.jpg][1]\r\n\r\n\r\n  [1]: http://172.16.1.167/usr/uploads/2017/12/1290185192.jpg', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(17, 'cabbc44c3fe2cc3d17dea0d07d4c955b168951c57bac-3BT2nq.jpg', 'cabbc44c3fe2cc3d17dea0d07d4c955b168951c57bac-3BT2nq-jpg', 1513952341, 1513952341, 'a:5:{s:4:"name";s:55:"cabbc44c3fe2cc3d17dea0d07d4c955b168951c57bac-3BT2nq.jpg";s:4:"path";s:35:"/usr/uploads/2017/12/3569984890.jpg";s:4:"size";i:31660;s:4:"type";s:3:"jpg";s:4:"mime";s:10:"image/jpeg";}', 1, 1, NULL, 'attachment', 'publish', NULL, 0, '1', '0', '1', 18, 0),
(18, 'fuli7', '18', 1513952346, 1513952346, '<!--markdown-->![cabbc44c3fe2cc3d17dea0d07d4c955b168951c57bac-3BT2nq.jpg][1]\r\n\r\n\r\n  [1]: http://172.16.1.167/usr/uploads/2017/12/3569984890.jpg', 0, 1, NULL, 'post', 'publish', NULL, 0, '1', '1', '1', 0, 0),
(20, 'fuli8', '20', 1513952340, 1514797479, '<!--markdown-->fuli8\r\n\r\n', 0, 1, NULL, 'post', 'publish', NULL, 1, '1', '1', '1', 0, 0),
(21, 'test', '21', 1514533411, 1514533411, '<!--markdown-->你好，美女\r\n\r\n\r\n\r\n解压密码：[hide]我是密码[/hide]', 0, 1, NULL, 'post', 'publish', NULL, 2, '1', '1', '1', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `typecho_fields`
--

CREATE TABLE IF NOT EXISTS `typecho_fields` (
  `cid` int(10) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` varchar(8) DEFAULT 'str',
  `str_value` text,
  `int_value` int(10) DEFAULT '0',
  `float_value` float DEFAULT '0',
  PRIMARY KEY (`cid`,`name`),
  KEY `int_value` (`int_value`),
  KEY `float_value` (`float_value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `typecho_invitation`
--

CREATE TABLE IF NOT EXISTS `typecho_invitation` (
  `inviteId` int(10) NOT NULL AUTO_INCREMENT,
  `inviteCode` varchar(64) NOT NULL,
  `inviteMail` varchar(200) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`inviteId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `typecho_invitation`
--

INSERT INTO `typecho_invitation` (`inviteId`, `inviteCode`, `inviteMail`, `status`) VALUES
(1, 'abcdef', 'admin@jingPinWu.com', 1);

-- --------------------------------------------------------

--
-- 表的结构 `typecho_metas`
--

CREATE TABLE IF NOT EXISTS `typecho_metas` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `count` int(10) unsigned DEFAULT '0',
  `order` int(10) unsigned DEFAULT '0',
  `parent` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`mid`),
  KEY `slug` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `typecho_metas`
--

INSERT INTO `typecho_metas` (`mid`, `name`, `slug`, `type`, `description`, `count`, `order`, `parent`) VALUES
(1, 'AV', 'av', 'category', '三次元', 9, 1, 0),
(2, 'ACG', 'acg', 'category', '二次元', 1, 2, 0);

-- --------------------------------------------------------

--
-- 表的结构 `typecho_options`
--

CREATE TABLE IF NOT EXISTS `typecho_options` (
  `name` varchar(32) NOT NULL,
  `user` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text,
  PRIMARY KEY (`name`,`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `typecho_options`
--

INSERT INTO `typecho_options` (`name`, `user`, `value`) VALUES
('theme', 0, 'JingPinWu'),
('timezone', 0, '28800'),
('lang', 0, NULL),
('charset', 0, 'UTF-8'),
('contentType', 0, 'text/html'),
('gzip', 0, '0'),
('generator', 0, 'JingPinWu 1.1/17.10.30'),
('title', 0, 'JingPinWu'),
('description', 0, 'JingPinWu'),
('keywords', 0, NULL),
('rewrite', 0, '1'),
('frontPage', 0, 'recent'),
('frontArchive', 0, '0'),
('commentsRequireMail', 0, '1'),
('commentsWhitelist', 0, '0'),
('commentsRequireURL', 0, '0'),
('commentsRequireModeration', 0, '0'),
('plugins', 0, 'a:2:{s:9:"activated";a:1:{s:7:"Account";a:0:{}}s:7:"handles";a:0:{}}'),
('commentDateFormat', 0, 'F jS, Y \\a\\t h:i a'),
('siteUrl', 0, 'http://172.16.1.167'),
('defaultCategory', 0, '1'),
('allowRegister', 0, '1'),
('defaultAllowComment', 0, '1'),
('defaultAllowPing', 0, '1'),
('defaultAllowFeed', 0, '1'),
('pageSize', 0, '10'),
('postsListSize', 0, '10'),
('commentsListSize', 0, '10'),
('commentsHTMLTagAllowed', 0, NULL),
('postDateFormat', 0, 'Y-m-d'),
('feedFullText', 0, '1'),
('editorSize', 0, '350'),
('autoSave', 0, '0'),
('markdown', 0, '1'),
('xmlrpcMarkdown', 0, '0'),
('commentsMaxNestingLevels', 0, '5'),
('commentsPostTimeout', 0, '2592000'),
('commentsUrlNofollow', 0, '1'),
('commentsShowUrl', 0, '1'),
('commentsMarkdown', 0, '0'),
('commentsPageBreak', 0, '0'),
('commentsThreaded', 0, '1'),
('commentsPageSize', 0, '20'),
('commentsPageDisplay', 0, 'last'),
('commentsOrder', 0, 'DESC'),
('commentsCheckReferer', 0, '1'),
('commentsAutoClose', 0, '0'),
('commentsPostIntervalEnable', 0, '0'),
('commentsPostInterval', 0, '60'),
('commentsShowCommentOnly', 0, '0'),
('commentsAvatar', 0, '1'),
('commentsAvatarRating', 0, 'G'),
('commentsAntiSpam', 0, '0'),
('routingTable', 0, 'a:28:{i:0;a:27:{s:5:"index";a:6:{s:3:"url";s:1:"/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:8:"|^[/]?$|";s:6:"format";s:1:"/";s:6:"params";a:0:{}}s:7:"archive";a:6:{s:3:"url";s:6:"/blog/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:13:"|^/blog[/]?$|";s:6:"format";s:6:"/blog/";s:6:"params";a:0:{}}s:2:"do";a:6:{s:3:"url";s:22:"/action/[action:alpha]";s:6:"widget";s:9:"Widget_Do";s:6:"action";s:6:"action";s:4:"regx";s:32:"|^/action/([_0-9a-zA-Z-]+)[/]?$|";s:6:"format";s:10:"/action/%s";s:6:"params";a:1:{i:0;s:6:"action";}}s:4:"post";a:6:{s:3:"url";s:23:"/[category]/[slug].html";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:30:"|^/([^/]+)/([^/]+)\\.html[/]?$|";s:6:"format";s:11:"/%s/%s.html";s:6:"params";a:2:{i:0;s:8:"category";i:1;s:4:"slug";}}s:10:"attachment";a:6:{s:3:"url";s:26:"/attachment/[cid:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:28:"|^/attachment/([0-9]+)[/]?$|";s:6:"format";s:15:"/attachment/%s/";s:6:"params";a:1:{i:0;s:3:"cid";}}s:8:"category";a:6:{s:3:"url";s:17:"/category/[slug]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:25:"|^/category/([^/]+)[/]?$|";s:6:"format";s:13:"/category/%s/";s:6:"params";a:1:{i:0;s:4:"slug";}}s:3:"tag";a:6:{s:3:"url";s:12:"/tag/[slug]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:20:"|^/tag/([^/]+)[/]?$|";s:6:"format";s:8:"/tag/%s/";s:6:"params";a:1:{i:0;s:4:"slug";}}s:6:"author";a:6:{s:3:"url";s:22:"/author/[uid:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:24:"|^/author/([0-9]+)[/]?$|";s:6:"format";s:11:"/author/%s/";s:6:"params";a:1:{i:0;s:3:"uid";}}s:6:"search";a:6:{s:3:"url";s:19:"/search/[keywords]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:23:"|^/search/([^/]+)[/]?$|";s:6:"format";s:11:"/search/%s/";s:6:"params";a:1:{i:0;s:8:"keywords";}}s:10:"index_page";a:6:{s:3:"url";s:21:"/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:22:"|^/page/([0-9]+)[/]?$|";s:6:"format";s:9:"/page/%s/";s:6:"params";a:1:{i:0;s:4:"page";}}s:12:"archive_page";a:6:{s:3:"url";s:26:"/blog/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:27:"|^/blog/page/([0-9]+)[/]?$|";s:6:"format";s:14:"/blog/page/%s/";s:6:"params";a:1:{i:0;s:4:"page";}}s:13:"category_page";a:6:{s:3:"url";s:32:"/category/[slug]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:34:"|^/category/([^/]+)/([0-9]+)[/]?$|";s:6:"format";s:16:"/category/%s/%s/";s:6:"params";a:2:{i:0;s:4:"slug";i:1;s:4:"page";}}s:8:"tag_page";a:6:{s:3:"url";s:27:"/tag/[slug]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:29:"|^/tag/([^/]+)/([0-9]+)[/]?$|";s:6:"format";s:11:"/tag/%s/%s/";s:6:"params";a:2:{i:0;s:4:"slug";i:1;s:4:"page";}}s:11:"author_page";a:6:{s:3:"url";s:37:"/author/[uid:digital]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:33:"|^/author/([0-9]+)/([0-9]+)[/]?$|";s:6:"format";s:14:"/author/%s/%s/";s:6:"params";a:2:{i:0;s:3:"uid";i:1;s:4:"page";}}s:11:"search_page";a:6:{s:3:"url";s:34:"/search/[keywords]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:32:"|^/search/([^/]+)/([0-9]+)[/]?$|";s:6:"format";s:14:"/search/%s/%s/";s:6:"params";a:2:{i:0;s:8:"keywords";i:1;s:4:"page";}}s:12:"archive_year";a:6:{s:3:"url";s:18:"/[year:digital:4]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:19:"|^/([0-9]{4})[/]?$|";s:6:"format";s:4:"/%s/";s:6:"params";a:1:{i:0;s:4:"year";}}s:13:"archive_month";a:6:{s:3:"url";s:36:"/[year:digital:4]/[month:digital:2]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:30:"|^/([0-9]{4})/([0-9]{2})[/]?$|";s:6:"format";s:7:"/%s/%s/";s:6:"params";a:2:{i:0;s:4:"year";i:1;s:5:"month";}}s:11:"archive_day";a:6:{s:3:"url";s:52:"/[year:digital:4]/[month:digital:2]/[day:digital:2]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:41:"|^/([0-9]{4})/([0-9]{2})/([0-9]{2})[/]?$|";s:6:"format";s:10:"/%s/%s/%s/";s:6:"params";a:3:{i:0;s:4:"year";i:1;s:5:"month";i:2;s:3:"day";}}s:17:"archive_year_page";a:6:{s:3:"url";s:38:"/[year:digital:4]/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:33:"|^/([0-9]{4})/page/([0-9]+)[/]?$|";s:6:"format";s:12:"/%s/page/%s/";s:6:"params";a:2:{i:0;s:4:"year";i:1;s:4:"page";}}s:18:"archive_month_page";a:6:{s:3:"url";s:56:"/[year:digital:4]/[month:digital:2]/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:44:"|^/([0-9]{4})/([0-9]{2})/page/([0-9]+)[/]?$|";s:6:"format";s:15:"/%s/%s/page/%s/";s:6:"params";a:3:{i:0;s:4:"year";i:1;s:5:"month";i:2;s:4:"page";}}s:16:"archive_day_page";a:6:{s:3:"url";s:72:"/[year:digital:4]/[month:digital:2]/[day:digital:2]/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:55:"|^/([0-9]{4})/([0-9]{2})/([0-9]{2})/page/([0-9]+)[/]?$|";s:6:"format";s:18:"/%s/%s/%s/page/%s/";s:6:"params";a:4:{i:0;s:4:"year";i:1;s:5:"month";i:2;s:3:"day";i:3;s:4:"page";}}s:12:"comment_page";a:6:{s:3:"url";s:53:"[permalink:string]/comment-page-[commentPage:digital]";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:36:"|^(.+)/comment\\-page\\-([0-9]+)[/]?$|";s:6:"format";s:18:"%s/comment-page-%s";s:6:"params";a:2:{i:0;s:9:"permalink";i:1;s:11:"commentPage";}}s:4:"feed";a:6:{s:3:"url";s:20:"/feed[feed:string:0]";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:4:"feed";s:4:"regx";s:17:"|^/feed(.*)[/]?$|";s:6:"format";s:7:"/feed%s";s:6:"params";a:1:{i:0;s:4:"feed";}}s:8:"feedback";a:6:{s:3:"url";s:31:"[permalink:string]/[type:alpha]";s:6:"widget";s:15:"Widget_Feedback";s:6:"action";s:6:"action";s:4:"regx";s:29:"|^(.+)/([_0-9a-zA-Z-]+)[/]?$|";s:6:"format";s:5:"%s/%s";s:6:"params";a:2:{i:0;s:9:"permalink";i:1;s:4:"type";}}s:4:"page";a:6:{s:3:"url";s:12:"/[slug].html";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";s:4:"regx";s:22:"|^/([^/]+)\\.html[/]?$|";s:6:"format";s:8:"/%s.html";s:6:"params";a:1:{i:0;s:4:"slug";}}s:14:"account_forgot";a:6:{s:3:"url";s:15:"/account/forgot";s:6:"widget";s:14:"Account_Widget";s:6:"action";s:8:"doForgot";s:4:"regx";s:23:"|^/account/forgot[/]?$|";s:6:"format";s:15:"/account/forgot";s:6:"params";a:0:{}}s:13:"account_reset";a:6:{s:3:"url";s:14:"/account/reset";s:6:"widget";s:14:"Account_Widget";s:6:"action";s:7:"doReset";s:4:"regx";s:22:"|^/account/reset[/]?$|";s:6:"format";s:14:"/account/reset";s:6:"params";a:0:{}}}s:5:"index";a:3:{s:3:"url";s:1:"/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:7:"archive";a:3:{s:3:"url";s:6:"/blog/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:2:"do";a:3:{s:3:"url";s:22:"/action/[action:alpha]";s:6:"widget";s:9:"Widget_Do";s:6:"action";s:6:"action";}s:4:"post";a:3:{s:3:"url";s:23:"/[category]/[slug].html";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:10:"attachment";a:3:{s:3:"url";s:26:"/attachment/[cid:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:8:"category";a:3:{s:3:"url";s:17:"/category/[slug]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:3:"tag";a:3:{s:3:"url";s:12:"/tag/[slug]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:6:"author";a:3:{s:3:"url";s:22:"/author/[uid:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:6:"search";a:3:{s:3:"url";s:19:"/search/[keywords]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:10:"index_page";a:3:{s:3:"url";s:21:"/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:12:"archive_page";a:3:{s:3:"url";s:26:"/blog/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:13:"category_page";a:3:{s:3:"url";s:32:"/category/[slug]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:8:"tag_page";a:3:{s:3:"url";s:27:"/tag/[slug]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:11:"author_page";a:3:{s:3:"url";s:37:"/author/[uid:digital]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:11:"search_page";a:3:{s:3:"url";s:34:"/search/[keywords]/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:12:"archive_year";a:3:{s:3:"url";s:18:"/[year:digital:4]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:13:"archive_month";a:3:{s:3:"url";s:36:"/[year:digital:4]/[month:digital:2]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:11:"archive_day";a:3:{s:3:"url";s:52:"/[year:digital:4]/[month:digital:2]/[day:digital:2]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:17:"archive_year_page";a:3:{s:3:"url";s:38:"/[year:digital:4]/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:18:"archive_month_page";a:3:{s:3:"url";s:56:"/[year:digital:4]/[month:digital:2]/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:16:"archive_day_page";a:3:{s:3:"url";s:72:"/[year:digital:4]/[month:digital:2]/[day:digital:2]/page/[page:digital]/";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:12:"comment_page";a:3:{s:3:"url";s:53:"[permalink:string]/comment-page-[commentPage:digital]";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:4:"feed";a:3:{s:3:"url";s:20:"/feed[feed:string:0]";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:4:"feed";}s:8:"feedback";a:3:{s:3:"url";s:31:"[permalink:string]/[type:alpha]";s:6:"widget";s:15:"Widget_Feedback";s:6:"action";s:6:"action";}s:4:"page";a:3:{s:3:"url";s:12:"/[slug].html";s:6:"widget";s:14:"Widget_Archive";s:6:"action";s:6:"render";}s:14:"account_forgot";a:3:{s:3:"url";s:15:"/account/forgot";s:6:"widget";s:14:"Account_Widget";s:6:"action";s:8:"doForgot";}s:13:"account_reset";a:3:{s:3:"url";s:14:"/account/reset";s:6:"widget";s:14:"Account_Widget";s:6:"action";s:7:"doReset";}}'),
('actionTable', 0, 'a:0:{}'),
('panelTable', 0, 'a:0:{}'),
('attachmentTypes', 0, '@image@'),
('secret', 0, 'gzlwX%qws$8Uf^GHUm*o4q3JLW8n2f(K'),
('installed', 0, '1'),
('allowXmlRpc', 0, '0'),
('theme:JingPinWu', 0, 'a:17:{s:7:"favicon";s:0:"";s:7:"logoUrl";s:0:"";s:8:"Homeflag";s:1:"1";s:5:"bgpic";s:0:"";s:6:"bgtext";s:101:"专注高质量内容。</b> <b class="main-font text-large font-weight-400">无精品，毋宁死。";s:10:"index_flag";s:5:"three";s:12:"archive_flag";s:4:"four";s:9:"sidebarlr";s:4:"full";s:12:"sidebarBlock";a:5:{i:0;s:12:"ShowComments";i:1;s:11:"ShowGuidang";i:2;s:10:"ShowRecent";i:3;s:12:"ShowCategory";i:4;s:8:"ShowTags";}s:12:"socialweixin";s:0:"";s:11:"socialweibo";s:27:"https://weibo.com/tombadmin";s:13:"socialtwitter";s:35:"https://twitter.com/beijinglaolarou";s:14:"socialfacebook";s:0:"";s:9:"socialrss";s:0:"";s:7:"src_add";s:0:"";s:7:"cdn_add";s:0:"";s:8:"footlink";s:0:"";}'),
('theme:H-Code', 0, 'a:17:{s:7:"favicon";N;s:7:"logoUrl";N;s:8:"Homeflag";s:1:"1";s:5:"bgpic";N;s:6:"bgtext";N;s:10:"index_flag";s:4:"four";s:12:"archive_flag";s:4:"four";s:9:"sidebarlr";s:5:"right";s:12:"sidebarBlock";a:5:{i:0;s:12:"ShowComments";i:1;s:11:"ShowGuidang";i:2;s:10:"ShowRecent";i:3;s:12:"ShowCategory";i:4;s:8:"ShowTags";}s:12:"socialweixin";N;s:11:"socialweibo";N;s:13:"socialtwitter";N;s:14:"socialfacebook";N;s:9:"socialrss";N;s:7:"src_add";N;s:7:"cdn_add";N;s:8:"footlink";N;}'),
('plugin:Account', 0, 'a:5:{s:4:"host";s:12:"smtp.126.com";s:4:"port";s:3:"465";s:8:"username";s:17:"tombadmin@126.com";s:8:"password";s:13:"dullub1995526";s:6:"secure";s:3:"ssl";}');

-- --------------------------------------------------------

--
-- 表的结构 `typecho_relationships`
--

CREATE TABLE IF NOT EXISTS `typecho_relationships` (
  `cid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cid`,`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `typecho_relationships`
--

INSERT INTO `typecho_relationships` (`cid`, `mid`) VALUES
(3, 1),
(4, 1),
(9, 1),
(12, 1),
(14, 1),
(16, 1),
(18, 1),
(20, 1),
(21, 1),
(22, 2);

-- --------------------------------------------------------

--
-- 表的结构 `typecho_users`
--

CREATE TABLE IF NOT EXISTS `typecho_users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `screenName` varchar(32) DEFAULT NULL,
  `created` int(10) unsigned DEFAULT '0',
  `activated` int(10) unsigned DEFAULT '0',
  `logged` int(10) unsigned DEFAULT '0',
  `group` varchar(16) DEFAULT 'visitor',
  `authCode` varchar(64) DEFAULT NULL,
  `invitation` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `typecho_users`
--

INSERT INTO `typecho_users` (`uid`, `name`, `password`, `mail`, `url`, `screenName`, `created`, `activated`, `logged`, `group`, `authCode`, `invitation`) VALUES
(1, 'tanyunfei', '$P$B2EigdEqFNELsaEzu0H.GR6eMKGiSR.', 'webmaster@yourdomain.com', 'http://www.google.com', 'RoadKiller', 1512974321, 1514816535, 1514816326, 'administrator', '3774bdc12522c5c403c57ee13a3b10f9', NULL),
(2, 'fuckwulytyf', '$P$BZ/0miio6/5ok1hRAsAK2r00gMApVh1', 'fvckwuly@gmail.com', NULL, 'fuckwuly', 1513855660, 1513859922, 1513855713, 'subscriber', 'c938aa0fdfda49fe68df2ba4a41a72e6', NULL),
(3, 'test126hehe', '$P$Bm4ebFgyLz8PKagyuJX8u/6wh4Pgcy/', 'tombadmin@126.com', NULL, 'test126', 1513858738, 1513860105, 1513858756, 'subscriber', 'a2dc35e1b9dfff063964556d857be0c9', NULL),
(4, 'testgooglehehe', '$P$BfUZIzbNgJFOTqjcJ1dgPNNwDn16CB0', 'fengfrederick@gmail.com', NULL, 'testgoogle', 1513866248, 0, 0, 'subscriber', NULL, NULL),
(5, 'testqqhehe', '$P$BHuacuSgUz34wOQees2zho3ZBd927O1', '205643149@qq.com', NULL, 'testqq', 1513866411, 0, 0, 'subscriber', NULL, NULL),
(6, 'fuckwuly123', '$P$Bx8QdRTQbiTF2rXsVs59/nW1oouzjM/', 'fvckwuly123@gmail.com', NULL, 'fuckwuly123', 1514518289, 1514518382, 0, 'subscriber', '55fb25065ba74ddb95fe7a9e8760be74', NULL),
(7, 'testgoogle123', '$P$BY0RLNI63I4MwWsWqQz/zoMJlTyr9H.', 'test@qq.com', NULL, 'testgoogle123', 1514518414, 1514519547, 0, 'subscriber', '32aa919ddc1b3c180e3565bca08f92b5', NULL),
(8, 'testinvitehehe', '$P$Bd1av7SWJ/1il0QTNtkWoZNYSOXysY.', 'testinvite@qq.com', NULL, 'testinvite', 1514519585, 1514520565, 0, 'subscriber', '4d5751859be6ccd1644dbb5a00c5a787', 'abc'),
(9, 'testinvite1', '$P$BPBwmquTCADRfNWdlehoFvTD8ZztuX0', 'testinvite1@qq.com', NULL, 'testinvite1', 1514520712, 1514520884, 0, 'subscriber', '905f20d8e75f87de331e695359794a23', 'abcdef'),
(10, 'admin1376985', '$P$BBZWjwFU1vGCPV8xeSjmXSNAJvxoj9.', 'admin1@qq.com', NULL, 'admin1', 1514520947, 1514521921, 0, 'subscriber', '1de251944a1b507fa202951cafc16b29', 'abcdef'),
(14, 'admin3213612', '$P$BgR4jqGwTUok9Thl0eUwlFBV7euKOx/', 'admin@JingPinWu.com', NULL, 'admin3', 1514538154, 1514538929, 0, 'contributor', '6f4278ae35eedbf0ad1e31925bf70a6e', 'abcdef');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
