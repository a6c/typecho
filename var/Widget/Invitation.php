<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;
require_once __TYPECHO_ROOT_DIR__ . '/usr/plugins/Account/PHPMailer/PHPMailerAutoload.php';

/**
 * 邀请码组件
 *
 * @author fuckccp
 * @category typecho
 * @package Widget
 */
 
class Widget_Invitation extends Widget_Abstract_Users implements Widget_Interface_Do
{

    public function action()
    {
        $this->security->protect(); //请求token校验
        if ($this->user->hasLogin() || !$this->options->allowRegister) {
            $this->response->redirect($this->options->index);
        }
		
		$validatorMail = new Typecho_Validate();
        $validatorMail->addRule('mail', 'required', _t('必须填写电子邮箱'));
		$validatorMail->addRule('mail', 'xssCheck', _t('请不要使用特殊字符'));
        $validatorMail->addRule('mail', 'email', _t('电子邮箱格式错误'));
        $validatorMail->addRule('mail', array($this, 'mailExists'), _t('电子邮箱地址已经存在'));
        $validatorMail->addRule('mail', 'maxLength', _t('电子邮箱最多包含200个字符'), 200);

		
        /** 截获验证异常 */
        if ($errorMail = $validatorMail->run($this->request->from(mail))) {
            Typecho_Cookie::set('__typecho_remember_mail', $this->request->mail);
            $this->widget('Widget_Notice')->set($errorMail);
            $this->response->goBack();
        }
		
		$validatorToken = new Typecho_Validate();
		$validatorToken->addRule('token', 'required', _t('必须填写支付校验码'));
        $validatorToken->addRule('token', 'minLength', _t('支付校验码错误'), 7);
        $validatorToken->addRule('token', 'maxLength', _t('支付校验码错误'), 8);
        /** 如果请求中有password */
//        if (array_key_exists('token', $_REQUEST)) {
         //$validator->addRule('confirm', 'confirm', _t('两次输入的密码不一致'), 'password');
//        }
		if ($errorToken = $validatorToken->run($this->request->from('token'))) {
            Typecho_Cookie::set('__typecho_remember_mail', $this->request->mail);
            $this->widget('Widget_Notice')->set($errorToken);
            $this->response->goBack();
        }
		
		$tokenCode = '1234567';

		if ($tokenCode !== $this->request->token) {
			Typecho_Cookie::set('__typecho_remember_mail', $this->request->mail);
			$this->widget('Widget_Notice')->set(_t('支付校验码错误'));
			$this->response->goBack();
			}
			
		
		$hasher = new PasswordHash(8, true);
		$payTime = time();
		$paySalt = $hasher->HashPassword($this->request->mail . $this->request->token . $payTime);
		$insertPay = $this->db->insert('table.pay')->rows(array('payMail' => $this->request->mail, 'payToken' => $this->request->token, 'paySalt' => $paySalt, 'payTime' => $payTime));
		$insertPayId = $this->db->query($insertPay);//将构建好的sql执行, 如果你的主键id是自增型的还会返回insert id

		if (!$insertPayId) {
			Typecho_Cookie::set('__typecho_remember_mail', $this->request->mail);
			$this->widget('Widget_Notice')->set(_t('支付状态获取失败，未能支付成功，请检查网络连接或尝试重新支付'));
			$this->response->goBack();
		}
		
		else {
			$inviteCodeHash = hash(sha1, hash(sha512, hash(md5, $insertPayId . $this->request->mail . $paySalt)));
			$inviteCodeSource = substr($inviteCodeHash, 0, 4) . substr($inviteCodeHash, 35);
			$inviteCode = strtoupper(substr($inviteCodeHash, 0, 3) .'-'. substr($inviteCodeHash, 3, 3) .'-'. substr($inviteCodeHash, 6, 3));

			$insertInvitation = $this->db->insert('table.invitation')->rows(array('inviteCode' => $inviteCode, 'inviteMail' => $this->request->mail, 'codeStatus' => 1, 'payToken' => $this->request->token, 'payId' => $insertPayId));
			$insertInviteId = $this->db->query($insertInvitation);
//			echo $insertInviteId;
/* 生成直接注册激活链接 */
            $registerCodeString = base64_encode(base64_encode(base64_encode($this->request->mail . '.' . $inviteCode)));
			$registerCodeUrl = Typecho_Common::url('/action/register.php?token=' . $registerCodeString, $this->options->index);
	        $this->config = $this->options->plugin('Account');

			$hashValidate = Typecho_Common::hash($hashString);
            $token = base64_encode($user['uid'] . '.' . $hashValidate . '.' . $this->options->gmtTime);
            $url = Typecho_Common::url('/action/register?token=' . $token, $this->options->index);

			

            /* 发送重置密码地址 */

            $invitationMailer = new PHPMailer();

            /* SMTP设置 */
            $invitationMailer->isSMTP();
            $invitationMailer->SMTPAuth = true;
            $invitationMailer->Host = $this->config->host;
            $invitationMailer->Port = $this->config->port;
            $invitationMailer->Username = $this->config->username;
            $invitationMailer->Password = $this->config->password;
            $invitationMailer->isHTML(true);
			$invitationMailer->CharSet = "UTF-8";

            if ('none' != $this->config->secure) {
                $invitationMailer->SMTPSecure = $this->config->secure;
            }

            $invitationMailer->setFrom($this->config->username, $this->options->title);
            $invitationMailer->addAddress('tombadmin@126.com', $this->request->mail);

            $invitationMailer->Subject = 'Invitation';
            $invitationMailer->Body    = '<p>' . $this->request->mail . ' 您好，您的邀请码是' . $inviteCode . '请点击以下链接完成注册</p>'
                . '<p> <a href="' . $registerCodeUrl . '">' . $registerCodeUrl . '</a>';

            if(!$invitationMailer->send()) {
                
				//恭喜您，赞助成功，精品污十分感谢有您的参与，在您的贡献下，精品污能够走得更远，精品污也将陪伴所有会员更久:)
				//您的精品污会员邀请码是$inviteCode，您可以点我进入注册页面进行注册。	//redirect(register_url);
			
				
				
				
				$this->widget('Widget_Notice')->set(_t('由于网络原因，邮件发送失败, 请重试或联系站长'), 'error');
            } 
			
			else {
				$this->widget('Widget_Notice')->set(_t('邀请码已成功发送至<strong>%s</strong>这个邮箱，请注意查收', $this->request->mail), 'success');
				$this->response->redirect($registerCodeUrl);

				//恭喜您，赞助成功，精品污十分感谢有您的参与，在您的贡献下，精品污能够走得更远，精品污也将陪伴所有会员更久:)
				//您的精品污会员邀请码是$inviteCode，您可以点我进入注册页面进行注册。
				//同时，我们已经向您的注册邮箱xxx@qq.com发送了会员邀请邮件，您可以通过您的邮箱查收您的邀请码，也可以通过点击邀请邮件中的链接快速完成注册，
				//如遇到其它问题，请联系我们的客服邮箱xxx@jingpinwu.com_addref
				//若仍没收到邮件，请点此重新发送
				
				
            }
		}
	}
}