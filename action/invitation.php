<?php
include 'common.php';

if ($user->hasLogin() || !$options->allowRegister) {
    $response->redirect($options->siteUrl);
}


$rememberMail = htmlspecialchars(Typecho_Cookie::get('__typecho_remember_mail'));
Typecho_Cookie::delete('__typecho_remember_mail');

$bodyClass = 'body-100';

include 'header.php';

Helper::addRoute('invitation', '/action/invitation', 'Widget_Invitation', 'action');



?>
<div class="typecho-login-wrap">
    <div class="typecho-login">
        <!--<h1><a href="http://typecho.org" class="i-logo">JingPinWu</a></h1>-->
        <form action="<?php $options->invitationAction(); ?>" method="post" name="invitation" role="form">

            <p>
                <label for="mail" class="sr-only"><?php _e('Email'); ?></label>
                <input type="email" id="mail" name="mail" placeholder="<?php _e('Email'); ?>" value="<?php echo $rememberMail; ?>" class="text-l w-100" autofocus />
            </p>
			<p>
                <label for="token" class="sr-only"><?php _e('支付校验码'); ?></label>
                <input type="password" id="token" name="token" placeholder="<?php _e('支付校验码'); ?>" value="" class="text-l w-100" />
            </p>


			<p class="invitation">
                <button type="submit" class="btn btn-l w-100 invite"><?php _e('获取邀请码'); ?></button>
            </p>
        </form>
        
        <p class="more-link">
            <a href="<?php $options->siteUrl(); ?>"><?php _e('返回首页'); ?></a>
            &bull;
            <a href="<?php $options->adminUrl('login.php'); ?>"><?php _e('用户登录'); ?></a>
			&bull;
            <?php
				$activates = array_keys(Typecho_Plugin::export()['activated']);
				if (in_array('Account', $activates)) {
					echo '<a href="' . Typecho_Common::url('account/forgot', $options->index) . '">' . '忘记密码' . '</a>';
					}
			?>
        </p>
    </div>
</div>
<?php 
include 'common-js.php';
?>
<script>
$(document).ready(function () {
    $('#name').focus();
});
</script>
<?php
include 'footer.php';
?>