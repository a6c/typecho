<?php


	function get_client_ip()
{
    $ip = 'unknown';
    $pattern = '/((?:(?:25[0-5]|2[0-4]\d|[01]?\d?\d)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d?\d))/';
    if(isset($_SERVER['REMOTE_ADDR']) && preg_match($pattern, $_SERVER['REMOTE_ADDR']))
    {
        $ip = $_SERVER['REMOTE_ADDR'];
    }else if(isset($_SERVER['HTTP_X_REAL_FORWARDED_FOR']) && preg_match($pattern, $_SERVER['HTTP_X_REAL_FORWARDED_FOR']))
    {
        $ip = $_SERVER['HTTP_X_REAL_FORWARDED_FOR'];
    }elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && preg_match($pattern, $_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }elseif(isset($_SERVER['HTTP_CLIENT_IP']) && preg_match($pattern, $_SERVER['HTTP_CLIENT_IP'])){
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    return $ip;
}


	//echo get_client_ip();
    //$RootDir = $_SERVER['DOCUMENT_ROOT'];
	
	function cn_ip_check($ip) {
    //$ip = $_SERVER["REMOTE_ADDR"];
	//$ip = '182.240.90.11';

	//echo $ip;

    sscanf($ip, "%d.%d.%d", $ip1, $ip2, $ip3);
    $ipv4arr = file("ipv4.txt");
    $cn = false;

    foreach ($ipv4arr as $ipv4)
    {
		//echo $ipv4;
        sscanf($ipv4, "%d.%d.%d.%d-%d.%d.%d.%d", $lip1, $lip2, $lip3, $lip4, $rip1, $rip2, $rip3, $rip4);
        if ($ip1 < $lip1 || $ip1 > $rip1)
            continue;
        if ($lip1 == $rip1)
        {
            if ($ip2 < $lip2 || $ip2 > $rip2)
                continue;
            if ($lip2 == $rip2)
            {
                if ($ip3 >= $lip3 && $ip3 <= $rip3)
                {
                    $cn = true;
                    break;
                }
            }
            else if ($ip2 == $lip2)
            {
                if ($ip3 >= $lip3)
                {
                    $cn = true;
                    break;
                }
            }
            else if ($ip2 == $rip2)
            {
                if ($ip3 <= $rip3)
                {
                    $cn = true;
                    break;
                }
            }
            else
            {
                $cn = true;
                break;
            }
        }
    }
	
	return $cn;
	
	//if ($cn) {
	//	return true;
	//}
    //else {
	//	return false;
	//}

}


if (cn_ip_check(get_client_ip())) {
	echo 'cn';
	}
else {
	echo 'error';
	}
	

?>